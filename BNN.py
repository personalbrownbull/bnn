# Brownbull Neural Network
import numpy as np
from components.functions.transfer import *

# BNN imports
from components.net.neuralNet import *

# MAIN
# n1 = Neuron(1,"Asd", 5)
# print(n1.toString())


# Network Architecture of hidden Layers
netMatrix = [
  ["Basic", "Basic", "Basic"], # Hidden Layer 1
  ["Basic", "Basic"] # Output Layer
]
# netMatrix = [
#   ["Basic", "Basic"],
#   ["Basic", "Basic", "Basic"],
#   ["Basic", "Basic", "Basic"],
#   ["Basic", "Basic"],
#   ["Basic", "Basic", "Basic","Basic"],
# ]


# DATA
# Define dataset
data = np.array([
  [ -2, -1],  # Alice 
  [ 25,  6],  # Bob
  [ 17,  4],  # Charlie
  [-15, -6],  # Diana
])
all_y_trues = np.array([
  [1, 0], # Alice
  [0, 1], # Bob
  [0, 1], # Charlie
  [1, 0], # Diana
])  
# print("index 0 Rows: {}".format(data.shape[0]))
# print("index 1 Cols: {}".format(data.shape[1]))

featuresQty = data.shape[1]
bnn = NeuralNet(inputQty = featuresQty, netMatrix = netMatrix)
print("*** Neural Net PRE Training ***")
print(bnn.toString())
print("*** Neural Net POST Training ***")
bnn.train(data, all_y_trues)
print(bnn.toString())

# EXEC NN
## Make some predictions
emily = np.array([-7, -3]) # 128 pounds, 63 inches
frank = np.array([20, 2])  # 155 pounds, 68 inches

# 1 output test
# print("Emily: %.3f" % bnn.feedForward(emily)) # 0.951 - F
# print("Frank: %.3f" % bnn.feedForward(frank)) # 0.039 - M

# 2 output test
print("Emily: {}".format(bnn.feedForward(emily))) # 0.951 - F
print("Frank: {}".format(bnn.feedForward(frank))) # 0.039 - M
# for idx, ppl in enumerate(data):
#   pred = bnn.feedForward(ppl)
#   print("{}: {}".format(idx, pred))

## DIAGRAM
driagramFolder = "data/graphs"
diagramName = "NNBasic"
bnn.graph.render('{}/{}'.format(driagramFolder, diagramName), view=True)  # doctest: +SKIP

