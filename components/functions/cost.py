# Cost Functions - Target to minimize on ANN

def mse_loss(y_true, y_pred):
  # y_true and y_pred are numpy arrays of the same length.
  return ((y_true - y_pred) ** 2).mean()

def deriv_mse_loss(y_true, y_pred):
  return -2 * (y_true - y_pred)

# Default Setup
defaultCostFunc, defaultCostFuncDeriv = mse_loss, deriv_mse_loss

# MAIN
def getCostFunctions(funcName):
  funcName = funcName.lower()
  if funcName == "mse":
    return mse_loss, deriv_mse_loss
  else:
    return defaultCostFunc, defaultCostFuncDeriv