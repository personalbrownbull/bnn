# Activation Functions
import numpy as np

def sigmoid(x):
  # Sigmoid activation function: f(x) = 1 / (1 + e^(-x))
  return 1 / (1 + np.exp(-x))

def deriv_sigmoid(x):
  # Sigmoid derivative function: f'(x) = f(x) * (1 - f(x))
  return sigmoid(x) * (1 - sigmoid(x))


# Default Setup
defaultTransferFunc, defaultTransferFuncDeriv = sigmoid, deriv_sigmoid

# MAIN
def getTransferFunctions(funcName):
  funcName = funcName.lower()
  if funcName == "sigmoid":
    return sigmoid, deriv_sigmoid
  else:
    return defaultTransferFunc, defaultTransferFuncDeriv