# Neural Network 11
from graphviz import Digraph
from components.net.layer.neuralLayer import *

def dirChar(value):
  return "↑" if value > 0.0 else "=" if value == 0.0 else "↓" 

def dirColor(value):
  return "#00b702" if value > 0.0 else "#c1aea1" if value == 0.0 else "#ff0000" 

folderCSV = "data/weights"
  
class NeuralNet:
  def __init__(self, inputQty = 1, netMatrix = [["Basic","Basic"],["Basic"]]):
    self.layers = []
    self.inputQty = inputQty
    self.miss = np.random.normal(size=inputQty)
    self.missTotal = 0.0
    # CREATE LAYERS
    inParmsQty = inputQty
    for idxLayer, layerMatrix in enumerate(netMatrix):
      newLayer = neuralLayer(idxLayer, inParmsQty, layerMatrix)
      inParmsQty = newLayer.outputQty
      self.layers.append(newLayer)
    self.outputQty = inParmsQty
    self.output = [0] * inParmsQty
    # Graph
    self.graph = Digraph('Nerual Network', filename='{}.gv'.format("NN"))
    self.graph.attr(rankdir='LR', size='8')

  def feedForward(self, inputRow):
    featuresQty = len(inputRow)
    features = np.copy(inputRow)
    if featuresQty != self.inputQty:
      print("{} features provided on input, while {} where expected.".format(featuresQty, self.inputQty))
      exit(400)

    for layer in self.layers:
      layer.feedForward(features)
      features = np.copy(layer.outputs)
    
    return features

  def backPropagate(self, expected):
    self.missTotal = 0.0
    # Iterate on nn layers from last to first
    for idxL in reversed(range(len(self.layers))):
      layer = self.layers[idxL]
      # Process output layer first 
      if idxL == (len(self.layers) - 1):
        for idxN, neuron in enumerate(layer.neurons):
          # print("### backProp: {} {}".format(idxN, expected[idxN]))
          # Getting error from expected values
          neuron.error = neuron.costFuncDeriv(expected[idxN], neuron.output)
          # Loss function 
          self.miss[idxN] = neuron.costFunc(expected[idxN], neuron.output)
          self.missTotal += self.miss[idxN]
      else:
        # Process hidden layers
        nextLayer = self.layers[idxL + 1]
        for idxN, neuron in enumerate(layer.neurons):
          neuron.error = 0.0
          for nextNeuron in nextLayer.neurons:
            # Get previous delta(error * derivTransf(activation)) and weights assoc to current neuron
            neuron.error += nextNeuron.delta * nextNeuron.weights[idxN]
      
      # consolidate Deltas
      for neuron in layer.neurons:
        neuron.delta = neuron.error * neuron.transferFuncDeriv(neuron.activation)

  def updateWeights(self, learnRate, learnDirection = -1):
    for idxL, layer in enumerate(self.layers):
      for idxN, neuron in enumerate(layer.neurons):
        neuron.saveHistoricWeights()
        for idxW in range(len(neuron.weights)):
          neuron.weights[idxW] += learnDirection * learnRate * neuron.delta * neuron.inputs[idxW]
        neuron.bias += learnDirection * learnRate * neuron.delta

  def train(self, data, all_y_trues, learn_rate = 0.1, epochs = 300, sampleEpoch = 50):
    for epoch in range(epochs):
      for x, y_true in zip(data, all_y_trues):
        y_pred = self.feedForward(x)
        self.backPropagate(y_true)
        self.updateWeights(learn_rate)

      # --- Calculate total loss at the end of each epoch
      if epoch % sampleEpoch == 0:
        print("Epoch %d loss: %.3f" % (epoch, self.missTotal))
        self.diagram(epoch, self.missTotal)
        for idxL, layer in enumerate(self.layers):
          for idxN, neuron in enumerate(layer.neurons):
            neuron.archiveWeights(epoch)
            neuron.historyToCsv("%s/epoch%s_L%s_N%s"% (folderCSV, epoch, idxL, idxN))

  # SEE
  def toString(self, level=0):
    strOut = "\nNeuralNet:"
    strOut += "\n> output({}): {}".format(self.outputQty, array2Str(self.output, floatDecimals = True))
    for layer in self.layers:
      strOut += layer.toString()
    return strOut

  def diagram(self, epoch, evaluateVal = 1.0, view=False):
    # with self.graph.subgraph(name='cluster_{}'.format(epoch), node_attr={'shape': 'record', 'height': '.01'}) as f:
    with self.graph.subgraph(name='cluster_{}'.format(epoch), node_attr={'shape': 'plaintext'}) as f:
      f.attr(rankdir='LR', size='13', label='epoch {} - {:10.5f}'.format(epoch, evaluateVal))

      # Attributes
      f.attr('edge', fontname='consolas', fontsize='10.0')
      f.attr('node', fontname='consolas', fontsize='8.0')
      
      for idxL, layer in enumerate(self.layers):
        # First layer defines input nodes - x
        if idxL == 0:
          inputHtml = '''<
            <TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="1">
              <TR> <TD>{0}</TD> <TD PORT="x">{1}</TD> </TR>
            </TABLE>>'''
          for idxIn, inp in enumerate(layer.inputs):
            f.node('%s_x%s'% (epoch, idxIn), inputHtml.format("x" + str(idxIn) , 'def'))
        # Neurons
        ## Nodes
        for idxN, neuron in enumerate(layer.neurons):
          ## Neuron table Header
          neuronHtml = '''<
            <TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="1" PORT="y">
              <TR> <TD>{0}</TD> <TD>val</TD> <TD>diff</TD> <TD>dir</TD> </TR>'''.format("n" + str(idxL) + str(idxN))
          ## Weight Rows
          for idxW in range(len(neuron.weights)):
            neuronHtml += '''<TR> <TD PORT="w{0}">w{0}</TD> <TD>{1:>5.2f}</TD> <TD>{2:>5.2f}</TD> <TD>{3}</TD> </TR>'''.format(
              idxW, neuron.weights[idxW], neuron.weightsDiffs[idxW], dirChar(neuron.weightsDiffs[idxW]))
          ## Bias Row
          neuronHtml += '''<TR> <TD        >bias</TD> <TD>{0:>5.2f}</TD> <TD>{1:>5.2f}</TD> <TD>{2}</TD> </TR>
              </TABLE>>'''.format(neuron.bias, neuron.biasDiff, dirChar(neuron.biasDiff))
          ## Show
          f.node('%s_h%s%s'% (epoch, idxL, idxN), neuronHtml)

        ## Edges
        for idxIn in range(len(layer.inputs)):
          for idxN, neuron in enumerate(layer.neurons):
            # x to neurons
            if idxL == 0:
                f.edge('%s_x%s:x'% (epoch, idxIn), '%s_h%s%s:w%d'% (epoch, idxL, idxN, idxIn), label="%s"% (dirChar(neuron.weightsDiffs[idxIn])), color=dirColor(neuron.weightsDiffs[idxIn]))
            # neuron connections
            else:
                f.edge('%s_h%s%s:y'% (epoch, idxL - 1, idxIn), '%s_h%s%s:w%d'% (epoch, idxL, idxN, idxIn), label="%s"% (dirChar(neuron.weightsDiffs[idxIn])), color=dirColor(neuron.weightsDiffs[idxIn]))
