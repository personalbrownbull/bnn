# Neuron in an Artificial Neuron Network
import numpy as np
from components.functions.transfer import *
from components.functions.cost import *

def neuronCreator(idxLayer, idxNeuron, inParms, neuronType = "basic"):
  neuronType = neuronType.lower()
  # Check and Create Specific Neuron Types
  if neuronType == "basic":
    return Neuron(
      layerId=idxLayer, 
      id="basic {}".format(idxNeuron), 
      inputQty=inParms)
  else:
    return Neuron(
      layerId=idxLayer, 
      id="basic {}".format(idxNeuron), 
      inputQty=inParms)

class Neuron:
  def __init__(self, layerId = 1, id = "null", inputQty = 1, transferName = "sigmoid", costName = "mse"):
    # ID
    self.layerId = layerId
    self.id = id
    # MAIN
    ## Core
    self.inputs = np.full((inputQty, 1), 0)
    self.weights = np.random.normal(size=inputQty)
    self.bias = np.random.normal()
    self.transferFunc, self.transferFuncDeriv = getTransferFunctions(transferName)
    self.costFunc, self.costFuncDeriv = getCostFunctions(costName)
    ## FeedForward
    self.activation = 0.0
    self.output = 0.0
    ## BackProp
    self.derivTransfer = 0.0
    self.delta = 0.0
    self.error = 0.0

    # ARCHIVE
    ## Values
    self.oldEpoch = 0
    self.oldWeights = self.weights
    self.oldBias = self.bias
    self.weightsHistory = []
    ## Diffs
    self.weightsDiffs = np.subtract(self.weights, self.oldWeights)
    self.biasDiff = self.bias - self.oldBias

  def predict(self, inputs):
    input = np.dot(self.weights, inputs)
    output = self.costFunc(input)
    return output

  def feedForward(self, inputs):
    self.inputs = np.copy(inputs)
    self.activation = np.dot(self.weights, inputs) + self.bias
    self.output = self.transferFunc(self.activation)
    # Archive
    self.biasDiff = self.bias - self.oldBias
    self.weightsDiffs = np.subtract(self.weights, self.oldWeights)

  # ARCHIVE
  def archiveWeights(self, epoch):
    self.oldEpoch = epoch
    self.oldBias = self.bias
    self.oldWeights = np.copy(self.weights)
    
  def saveHistoricWeights(self):
    self.weightsHistory.append(np.copy(self.weights))

  def historyToCsv(self, fileName):
    csvStr = ""
    delim = ","
    with open('%s.csv'% fileName,'w') as file:
      for epoch, wgths in enumerate(self.weightsHistory):
        # Header
        if epoch == 0: 
          csvStr += "epoch"
          for idx, w in enumerate(wgths):
            csvStr += (delim + "w%d"% idx)
          csvStr += "\n"
        # Data
        csvStr += str(epoch)
        for wgt in wgths:
          csvStr += (delim + "%.3f"% wgt)
        csvStr += "\n"
      # Save to csv
      file.write(csvStr)
    # Reset history
    self.weightsHistory = []

  # SEE
  def toString(self):
    strOut = "\n## Neuron {}\n##> Weights:".format(self.id)
    for idx, weight in enumerate(self.weights):
      strOut += "  w_{:>02d}:{:>6.3f}".format(idx, weight) 
    strOut += " bias: {:>6.3f}".format(self.bias)
    return strOut + "\n"