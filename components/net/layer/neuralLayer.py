# from https://stackoverflow.com/questions/30669474/beyond-top-level-package-error-in-relative-import
import sys
sys.path.append("..")
from brownpy.program import *

# Neural Layer
from components.net.layer.node.neuron import *

class neuralLayer:
  def __init__(self, idLayer = 0, inputQty = 1, layerMatrix = ["Basic"]):
    self.id = idLayer
    self.neurons = []
    self.inputs = []
    self.inputQty = inputQty
    self.outputs = []
    self.outputQty = len(layerMatrix)

    # CREATE NEURONS
    for idxNeuron, neuron in enumerate(layerMatrix):
      self.neurons.append(neuronCreator(self.id, idxNeuron, self.inputQty, neuronType = neuron))
  
  def feedForward(self, inputs):
    self.inputs = np.copy(inputs)
    self.inputQty = len(self.inputs)
    self.outputs = []
    for neuron in self.neurons:
      neuron.feedForward(self.inputs)
      self.outputs.append(neuron.output)
    self.outputQty = len(self.outputs)

  # SEE
  def toString(self, level=0):
    strOut = "\n# Neural Layer {}:".format(self.id)
    strOut += "\n#> inputs({}): {}".format(self.inputQty, array2Str(self.inputs, floatDecimals = True))
    strOut += "\n#> output({}): {}".format(self.outputQty, array2Str(self.outputs, floatDecimals = True))
    if level == 0:
      for neuron in self.neurons:
        strOut += neuron.toString()
    return strOut

    